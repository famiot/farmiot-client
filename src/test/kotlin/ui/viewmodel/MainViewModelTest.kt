package ui.viewmodel

import domain.actions.Connect
import domain.actions.SubscribeRead
import domain.actions.SubscribeStatus
import domain.model.Result
import domain.model.sensor.NodeStatus
import domain.model.sensor.SensorRead
import infrastructure.dto.NodeStatusDto
import infrastructure.dto.toSensorReadDto
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.kotlin.*
import utils.toDto
import kotlin.test.assertEquals

class MainViewModelTest {

    lateinit var mainViewModel: MainViewModel
    private val connect: Connect = mock()
    private val subscribeRead: SubscribeRead = mock()
    private val subscribeStatus: SubscribeStatus = mock()

    @BeforeEach
    fun setup() {
        mainViewModel = MainViewModel(
            connectAction = connect,
            subscribeReadAction = subscribeRead,
            subscribeStatusAction = subscribeStatus
        )
    }

    @Test
    fun `Should return a result value on successful connection`() {
        whenConnectSuccess()

        var result: Result = Result.Failure(Throwable("Test error"))
        mainViewModel.start {
            result = it
        }

        assertTrue(result is Result.Success<*>)
    }

    @Test
    fun `Should subscribe to topics on successful connection`() {
        whenConnectSuccess()

        mainViewModel.start {}

        verify(subscribeRead, atLeastOnce()).invoke(any(), any())
    }

    @Test
    fun `Should receive message from reads`() {
        var sensorRead: SensorRead? = null
        var callback: ((sensorRead: SensorRead?) -> Unit)?
        val expectedMessage = """{"sensor":"OTHER","node":"node1","data":{"type":"float","value":10.1}}"""
        val expectedSensorRead = expectedMessage.toSensorReadDto()?.toSensorRead()

        mainViewModel.onRead() {
            sensorRead = it
        }
        whenever(subscribeRead.invoke(any(), any())).thenAnswer {
            callback = it.getArgument(1)
            callback?.invoke(expectedSensorRead!!)
        }

        whenConnectSuccess()
        mainViewModel.start {}


        assertEquals(expectedSensorRead, sensorRead)
    }

    @Test
    fun `Should receive message from status`() {
        var nodeStatus: NodeStatus? = null
        var callback: ((nodeStatus: NodeStatus?) -> Unit)?
        val expectedMessage = """{"node":"node1","status" : "ONLINE"}"""
        val expectedNodeStatus = expectedMessage.toDto<NodeStatusDto>()?.toNodeStatus()

        mainViewModel.onStatus() {
            nodeStatus = it
        }
        whenever(subscribeStatus.invoke(any(), any())).thenAnswer {
            callback = it.getArgument(1)
            callback?.invoke(expectedNodeStatus)
        }

        whenConnectSuccess()
        mainViewModel.start {}


        assertEquals(expectedNodeStatus, nodeStatus)
    }

    private fun whenConnectSuccess() {
        whenever(connect.invoke(any())).thenAnswer {
            val callback: (result: Result) -> Unit = it.getArgument(0)
            callback.invoke(Result.Success(true))
        }
    }
}