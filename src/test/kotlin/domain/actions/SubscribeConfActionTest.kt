package domain.actions

import domain.MessageRepository
import domain.model.Result
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

class SubscribeConfActionTest {
    private val messageRepository: MessageRepository = mock()

    @Test
    fun subscribeAndRetrieveConfMap() {
        val response = """
            {
                "param_string":"string",
                "param_int": 0,
                "param_boolean": true
            }
        """.trimIndent()
        whenever(messageRepository.subscribe(any(), any())).thenAnswer {
            val callback: (message: Result) -> Unit = it.getArgument(1)
            callback.invoke(Result.Success(response))
        }
        val subscribeConfAction = SubscribeConfAction(messageRepository)
        var mapResponse = mapOf<String, Any>()
        subscribeConfAction.invoke("any_topic") { result ->
            result?.let { mapResponse = it }
        }
        assertEquals("string", mapResponse["param_string"])
        assertEquals(0, mapResponse["param_int"])
        assertEquals(true, mapResponse["param_boolean"])
    }

}