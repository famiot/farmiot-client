import domain.actions.PublishMessageAction
import domain.MessageRepository
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.kotlin.*

class PublishMessageTest {

    private val messageRepository: MessageRepository = mock()

    @Test
    fun `should publish a message`() {
        val topic = "some/topic"
        val message = "some_message"
        whenever(messageRepository.publish(eq(topic), eq(message), any())).thenAnswer {
            val callback: (result: domain.model.Result.Success<Boolean>) -> Unit = it.getArgument(2)
            callback.invoke(domain.model.Result.Success(true))
        }
        val publishMessage = PublishMessageAction(messageRepository)
        var myResult = false
        publishMessage(topic = topic, payload = message) { result ->
            myResult = result
        }

        verify(messageRepository).publish(eq(topic), eq(message), any())
        Assertions.assertEquals(true, myResult)
    }

}