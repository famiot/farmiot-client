package infrastructure.database

import domain.model.sensor.SensorRead
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class MemoryPersistenceRepositoryTest {

    lateinit var memoryPersistenceRepository: MemoryPersistenceRepository

    @BeforeEach
    fun setUp() {
        memoryPersistenceRepository = MemoryPersistenceRepository()
    }

    @Test
    fun `add and retrieve reading`() {
        memoryPersistenceRepository.addReading(SensorRead(0, "node1", "SOIL_HUMIDITY", 1.0))

        val reading = memoryPersistenceRepository.readingsBySensor("SOIL_HUMIDITY")

        Assertions.assertEquals(1, reading.size)
        Assertions.assertEquals(1.0, reading.first().value)
    }

    @Test
    fun `add and retrieve reading by epoch time`() {

        repeat(50) {
            memoryPersistenceRepository.addReading(SensorRead(it.toLong(), "node1", "SOIL_HUMIDITY", 1.0))
        }

        repeat(50) {
            memoryPersistenceRepository.addReading(SensorRead(50 + it.toLong(), "node1", "OTHER", 1.0))
        }

        val reading = memoryPersistenceRepository.readingsByDate(1, 50)

        Assertions.assertEquals(50, reading.size)
    }

    @Test
    fun `dump all records`() {
        repeat(50) {
            memoryPersistenceRepository.addReading(SensorRead(it.toLong(), "node1", "SOIL_HUMIDITY", 1.0))
        }
        val result = memoryPersistenceRepository.dump()
        val reading = memoryPersistenceRepository.readingsByDate(1, 50)

        Assertions.assertEquals(0, reading.size)
        Assertions.assertFalse(result.isEmpty())
    }


}