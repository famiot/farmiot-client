package infrastructure.database

import domain.model.sensor.Sensor
import domain.model.sensor.SensorRead
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertEquals
import utils.getEpochFromString
import utils.getStringFromEpoch
import utils.isBetweenTimes
import java.io.File

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class CsvPersistenceRepositoryTest {
    private var currentTime: Long = 0
    private val epochTime: () -> Long = {
        currentTime += 10L
        currentTime
    }
    private lateinit var csvPersistenceRepository: CsvPersistenceRepository
    private val folder = File("temp")

    @BeforeEach
    fun setUp() {
        folder.deleteRecursively()
        currentTime = 0
        csvPersistenceRepository = CsvPersistenceRepository(folder.name)
        currentTime = 10000000000
    }

    @AfterAll
    fun clean() {
        csvPersistenceRepository.dump()
    }

    @Test
    fun addReading() {
        givenReadingsInRepository("SOIL_HUMIDITY")

        val tree = folder.walkBottomUp().filter { it.isFile }.toList()
        assertEquals(1, tree.size)

    }

    @Test
    fun readingsBySensor() {
        givenReadingsInRepository("SOIL_HUMIDITY")
        givenReadingsInRepository("OTHER")

        val humidityReadings = csvPersistenceRepository.readingsBySensor("SOIL_HUMIDITY")

        assertEquals(100, humidityReadings.size)
    }

    @Test
    fun readingsByDate() {
        givenReadingsInRepository("OTHER")
        givenReadingsInRepository("SOIL_HUMIDITY")

        val humidityReadings = csvPersistenceRepository.readingsByDate(currentTime - 1000, currentTime)

        assertEquals(100, humidityReadings.size)
    }

    @Test
    fun dump() {
        givenReadingsInRepository("SOIL_HUMIDITY")

        csvPersistenceRepository.dump()
        val humidityReadings = csvPersistenceRepository.readingsBySensor("SOIL_HUMIDITY")

        assertEquals(0, humidityReadings.size)
    }

    @Test
    fun `should compare if a date time is between dates`() {
        val date = (currentTime - 500L).getStringFromEpoch()
        val isBetween = date.isBetweenTimes(currentTime - 1000L, currentTime)

        assertEquals(true, isBetween)
    }

    @Test
    fun `should decode date strings`() {
        val date = (currentTime).getStringFromEpoch()
        val epochTime = date.getEpochFromString()

        assertEquals(currentTime, epochTime)
    }

    private fun givenReadingsInRepository(sensor: Sensor) {
        val readings = mutableListOf<SensorRead>()
        repeat(100) {
            readings.add(SensorRead(epochTime(), "node1", sensor, 1.0))
        }
        csvPersistenceRepository.addReading(*readings.toTypedArray())
    }
}