package infrastructure.network.mqtt

import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Test
import kotlin.test.assertTrue

class FilterValidTopicTest {

    @Test
    fun `should validate the same topic`() {
        val filterValidTopic = FilterValidTopic()
        val topic = "sub1/sub2/sub3"
        val result = filterValidTopic(key = "sub1/sub2/sub3", topic)
        assertTrue(result)
    }

    @Test
    fun `should not validate the wrong topic`() {
        val filterValidTopic = FilterValidTopic()
        val topic = "sub1/sub2/sub3"
        val result = filterValidTopic(key = "sub1/sub2/sub4", topic)
        assertFalse(result)
    }

    @Test
    fun `should validate topic with one wildcard`() {
        val filterValidTopic = FilterValidTopic()
        val topic = "sub1/sub2/sub3"
        val result = filterValidTopic(key = "+/sub2/sub3", topic)
        assertTrue(result)
    }

    @Test
    fun `should validate topic with two wildcard`() {
        val filterValidTopic = FilterValidTopic()
        val topic = "sub1/sub2/sub3"
        val result = filterValidTopic(key = "+/sub2/+", topic)
        assertTrue(result)
    }
}