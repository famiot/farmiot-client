package infrastructure.network.mqtt

import domain.model.Result
import org.eclipse.paho.client.mqttv3.IMqttAsyncClient
import org.eclipse.paho.client.mqttv3.IMqttToken
import org.eclipse.paho.client.mqttv3.MqttMessage
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.mockito.kotlin.*
import java.nio.charset.Charset

class MqttMessageRepositoryTest {
    private val client: IMqttAsyncClient = mock()
    private val mqttMessageRepository = MqttMessageRepository(client)
    private val iMqttToken: IMqttToken = mock()
    private var result: Result? = null
    private val callback: (result: Result) -> Unit = {
        result = it
    }

    @Test
    fun connect() {
        whenever(iMqttToken.userContext).thenReturn(UserAction(MessageAction.CONNECT, callback))

        mqttMessageRepository.connect(callback)
        mqttMessageRepository.onSuccess(iMqttToken)

        assertTrue(result is Result.Success<*>)
        val success = result as Result.Success<*>
        assertEquals(true, success.value)
    }

    @Test
    fun disconnect() {
        whenever(iMqttToken.userContext).thenReturn(UserAction(MessageAction.DISCONNECT, callback))

        mqttMessageRepository.disconnect(callback)
        mqttMessageRepository.onSuccess(iMqttToken)

        assertTrue(result is Result.Success<*>)
        val success = result as Result.Success<*>
        assertEquals(true, success.value)
    }

    @Test
    fun isConnected() {
        whenever(client.isConnected).thenReturn(true)

        val isConnected = mqttMessageRepository.isConnected()

        assertEquals(true, isConnected)
    }

    @Test
    fun publishSuccess() {
        whenever(iMqttToken.userContext).thenReturn(UserAction(MessageAction.PUBLISH, callback))

        mqttMessageRepository.publish("some_topic", "some_payload", callback)
        mqttMessageRepository.onSuccess(iMqttToken)

        assertTrue(result is Result.Success<*>)
        val success = result as Result.Success<*>
        assertEquals(true, success.value)
    }

    @Test
    fun publishFailure() {
        whenever(iMqttToken.userContext).thenReturn(UserAction(MessageAction.PUBLISH, callback))

        mqttMessageRepository.publish("some_topic", "some_payload", callback)
        val exception = Throwable("error")
        mqttMessageRepository.onFailure(iMqttToken, exception)

        assertTrue(result is Result.Failure)
        val failure = result as Result.Failure
        assertEquals(exception, failure.exception)
    }

    @Test
    fun subscribe() {

        val topic = "some_topic"
        val message = "some_message"
        mqttMessageRepository.subscribe(topic, callback = callback)
        mqttMessageRepository.messageArrived(topic, MqttMessage(message.toByteArray(Charset.forName("UTF8"))))

        verify(client).subscribe(eq(arrayOf(topic)), any(), any(), any())
        assertTrue(result is Result.Success<*>, "Result is Success")
        val success = result as Result.Success<String>
        assertEquals(message, success.value)
        val otherMessage = "other_message"
        mqttMessageRepository.messageArrived(topic, MqttMessage(otherMessage.toByteArray(Charset.forName("UTF8"))))
        val otherSuccess = result as Result.Success<String>
        assertEquals(otherMessage, otherSuccess.value)
    }
}