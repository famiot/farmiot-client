package infrastructure.dto

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class SensorReadDtoTest {

    @Test
    fun `should return a valid object from a valid json`() {
        val json = """{"sensor":"SOIL_MOISTURE","node":"node1", "data":{"type":"float","value":10.2}}"""
        val sensorReadDto = json.toSensorReadDto()

        assertEquals(sensorReadDto?.sensor, "SOIL_MOISTURE")
        assertEquals(sensorReadDto?.data?.value, 10.2)
    }

    @Test
    fun `should return null from an invalid json`() {
        val json = """{}"""
        val sensorReadDto = json.toSensorReadDto()

        assertEquals(sensorReadDto, null)
    }
}