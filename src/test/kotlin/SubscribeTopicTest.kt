import domain.MessageRepository
import domain.actions.SubscribeReadAction
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.eq
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

class SubscribeTopicTest {

    private val messageRepository: MessageRepository = mock()

    @Test
    fun `should subscribe to a topic`() {
        val topic = "some/topic"
        val expectedMessage = """{"sensor":"OTHER","node":"node1","data":{"type":"float","value":10.1}}"""
        val subscribeTopic = SubscribeReadAction(messageRepository)
        whenever(messageRepository.subscribe(eq(topic), any())).thenAnswer {
            val callback: (message: domain.model.Result) -> Unit = it.getArgument(1)
            callback.invoke(domain.model.Result.Success(expectedMessage))
        }
        var message = 0.0
        subscribeTopic.invoke(topic = topic) {
            message = it?.value as Double
        }

        Assertions.assertEquals(10.1, message)
    }
}