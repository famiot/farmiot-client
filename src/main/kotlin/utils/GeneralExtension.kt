package utils

import com.fasterxml.jackson.module.kotlin.MissingKotlinParameterException
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.google.gson.JsonObject

inline fun <reified T> String.toDto(): T? {
    return try {
        jacksonObjectMapper().readValue(this)
    } catch (e: MissingKotlinParameterException) {
        null
    }
}

fun Any.format(precision: Int = 2): String {
    return if (this is Double || this is Float) String.format("%.${precision}f", this) else this.toString()
}

fun Any.isMajor(value: Float, defaultNotNumber: Boolean = false): Boolean {
    return if (this is Float || this is Double || this is Int) this.toNumber() > value else defaultNotNumber
}

fun Any.isMinor(value: Float, defaultNotNumber: Boolean = false): Boolean {
    return if (this is Float || this is Double || this is Int) this.toNumber() < value else defaultNotNumber
}

private fun Any.toNumber(): Double {
    return this.toString().toDouble()
}

fun JsonObject.fromMap(map: Map<String, Any>): JsonObject {
    map.forEach { (t, u) ->
        when (u) {
            is Int -> this.addProperty(t, u)
            is Boolean -> this.addProperty(t, u)
            is Float -> this.addProperty(t, u)
            is Double -> this.addProperty(t, u)
            else -> this.addProperty(t, u.toString())
        }
    }
    return this
}

fun String.toExpectedValue(value: Any): Any {
    return when (value) {
        is Int -> this.toInt()
        is Boolean -> this.toBoolean()
        is Float -> this.toFloat()
        is Double -> this.toDouble()
        else -> this
    }
}