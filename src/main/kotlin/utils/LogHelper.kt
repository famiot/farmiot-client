package utils

import org.slf4j.Logger
import org.slf4j.LoggerFactory

object LogHelper {
    val prefix = "@_"

    @JvmStatic
    fun i(tag: String, mensaje: String) {
        Log.i(prefix + tag, mensaje)
    }

    @JvmStatic
    fun e(tag: String, mensaje: String) {
        Log.e(prefix + tag, mensaje)
    }

    @JvmStatic
    fun w(tag: String, mensaje: String) {
        Log.w(prefix + tag, mensaje)
    }

    @JvmStatic
    fun d(tag: String, mensaje: String) {
        Log.d(prefix + tag, mensaje)
    }

    @JvmStatic
    fun e(tag: String, mensaje: String, t: Throwable) {
        e(tag, "$mensaje\n${t.message}")
    }


}

object Log {
    fun d(tag: String, msg: String) {
        log("DEBUG", tag, msg)
    }

    fun e(tag: String, msg: String) {
        log("ERROR", tag, msg)
    }

    fun i(tag: String, msg: String) {
        log("INFO", tag, msg)
    }

    fun w(tag: String, msg: String) {
        log("WARNING", tag, msg)
    }

    private fun log(type: String, tag: String, msg: String) {
        when (type) {
            "DEBUG" -> getLogger().debug("$tag : $msg")
            "ERROR" -> getLogger().error("$tag : $msg")
            "INFO" -> getLogger().info("$tag : $msg")
            "WARNING" -> getLogger().warn("$tag : $msg")
        }
    }


}

fun getLogger(): Logger = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME)