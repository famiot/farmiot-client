package utils

import java.text.SimpleDateFormat
import java.util.*

const val defaultFormat = "yyyyMMdd-HHmmss"

fun String.isBetweenTimes(from: Long, to: Long, format: String = defaultFormat): Boolean {
    val dateEpoch = this.getEpochFromString(format)
    return dateEpoch in from..to
}

fun String.getEpochFromString(format: String = defaultFormat): Long {
    val formatter = SimpleDateFormat(format)
    val date = formatter.parse(this)
    return date.time
}

fun Long.getStringFromEpoch(format: String = defaultFormat): String =
    try {
        SimpleDateFormat(format).format(Date(this))
    } catch (e: Exception) {
        "timeless"
    }


