package domain

import domain.model.sensor.Sensor
import domain.model.sensor.SensorRead

interface PersistenceRepository {
    fun addReading(vararg sensorRead: SensorRead)
    fun readingsBySensor(sensor: Sensor): List<SensorRead>
    fun readingsByDate(from: Long, to: Long): List<SensorRead>
    fun dump(): List<SensorRead>
    fun isFull(): Boolean
}