package domain

import domain.model.Result

interface MessageRepository {
    fun connect(callback: (result: Result) -> Unit = {})
    fun disconnect(callback: (result: Result) -> Unit = {})
    fun isConnected(): Boolean
    fun publish(topic: String, payload: String, callback: (Result) -> Unit = {})
    fun subscribe(topic: String, callback: (message: Result) -> Unit)
    fun unsubscribe(topic: String)
}
