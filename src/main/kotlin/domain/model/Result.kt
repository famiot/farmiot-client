package domain.model

sealed class Result {
    class Success<T>(val value: T) : Result()
    class Failure(val exception: Throwable) : Result()
}