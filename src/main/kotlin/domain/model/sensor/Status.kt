package domain.model.sensor


enum class Status {
    ONLINE,
    OFFLINE,
    UNKNOWN
}