package domain.model.sensor

import com.fasterxml.jackson.annotation.JsonPropertyOrder

typealias Sensor = String

@JsonPropertyOrder("timestamp", "value", "sensor")
data class SensorRead(val timestamp: Long, val node: Node, val sensor: Sensor, val value: Any)
