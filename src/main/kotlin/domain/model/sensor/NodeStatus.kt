package domain.model.sensor

import com.fasterxml.jackson.annotation.JsonPropertyOrder

typealias Node = String

@JsonPropertyOrder("timestamp", "node", "status")
data class NodeStatus(val timestamp: Long, val node: Node, val status: Status)