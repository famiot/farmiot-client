package domain.actions

import domain.MessageRepository
import domain.model.Result

interface PublishMessage {
    operator fun invoke(topic: String, payload: String, function: (result: Boolean) -> Unit)
}

class PublishMessageAction(private val messageRepository: MessageRepository) : PublishMessage {
    override fun invoke(topic: String, payload: String, function: (result: Boolean) -> Unit) {
        messageRepository.publish(topic = topic, payload = payload) {
            if (it is Result.Success<*>)
                function(it.value as Boolean)
        }
    }

}
