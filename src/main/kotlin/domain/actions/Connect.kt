package domain.actions

import domain.MessageRepository
import domain.model.Result

interface Connect{
    operator fun invoke(function: (result: Result) -> Unit)
}

class ConnectAction( private val messageRepository :MessageRepository):Connect  {

     override fun invoke(function: (result: Result) -> Unit) {
        if(!messageRepository.isConnected())
            messageRepository.connect(function)
        else
            function(Result.Success(true))
    }
}