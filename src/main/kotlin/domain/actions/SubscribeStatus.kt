package domain.actions

import domain.MessageRepository
import domain.model.Result
import domain.model.sensor.NodeStatus
import infrastructure.dto.NodeStatusDto
import utils.toDto

interface SubscribeStatus {
    operator fun invoke(topic: String, function: (message: NodeStatus?) -> Unit)
}

class SubscribeStatusAction(private val messageRepository: MessageRepository) : SubscribeStatus {
    override fun invoke(topic: String, function: (message: NodeStatus?) -> Unit) {
        messageRepository.subscribe(topic) {
            if (it is Result.Success<*> && it.value is String)
                function(it.value.toDto<NodeStatusDto>()?.toNodeStatus())
        }
    }

}
