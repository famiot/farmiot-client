package domain.actions

import domain.MessageRepository
import domain.model.Result

interface Disconnect{
    operator fun invoke(function: (result: Result) -> Unit)
}

class DisconnectAction( private val messageRepository :MessageRepository):Disconnect  {

     override fun invoke(function: (result: Result) -> Unit) {
        if(messageRepository.isConnected())
            messageRepository.disconnect(function)
        else
            function(Result.Success(true))
    }
}