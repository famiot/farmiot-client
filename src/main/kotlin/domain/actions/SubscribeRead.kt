package domain.actions

import domain.MessageRepository
import domain.model.Result
import domain.model.sensor.SensorRead
import infrastructure.dto.SensorReadDto
import utils.toDto

interface SubscribeRead {
    operator fun invoke(topic: String, function: (message: SensorRead?) -> Unit)
}

class SubscribeReadAction(private val messageRepository: MessageRepository) : SubscribeRead {
    override fun invoke(topic: String, function: (message: SensorRead?) -> Unit) {
        messageRepository.subscribe(topic) {
            if (it is Result.Success<*> && it.value is String)
                function(it.value.toDto<SensorReadDto>()?.toSensorRead())
        }
    }

}
