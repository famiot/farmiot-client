package domain.actions

import domain.MessageRepository
import domain.model.Result
import org.json.JSONObject

interface SubscribeConf {
    operator fun invoke(topic: String, function: (response: Map<String, Any>?) -> Unit)
}

class SubscribeConfAction(private val messageRepository: MessageRepository) : SubscribeConf {
    override fun invoke(topic: String, function: (response: Map<String, Any>?) -> Unit) {
        messageRepository.subscribe(topic) {
            if (it is Result.Success<*> && it.value is String) {
                function(JSONObject(it.value).toMap())
            }
        }
    }

}
