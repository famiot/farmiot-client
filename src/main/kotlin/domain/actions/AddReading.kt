package domain.actions

import domain.PersistenceRepository
import domain.model.sensor.SensorRead
import infrastructure.database.MemoryPersistenceRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AddReading(
    private val volatilePersistence: PersistenceRepository = MemoryPersistenceRepository(),
    private val persistenceRepository: PersistenceRepository
) {
    private val coroutineScope = CoroutineScope(Dispatchers.IO)
    operator fun invoke(sensorRead: SensorRead) {
        volatilePersistence.addReading(sensorRead)
        persists()
    }

    private fun persists() {
        coroutineScope.launch {
            if(volatilePersistence.isFull()){
                val volatile = volatilePersistence.dump()
                persistenceRepository.addReading(*volatile.toTypedArray())
            }
        }
    }
}