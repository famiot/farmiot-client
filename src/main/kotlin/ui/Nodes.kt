@file:OptIn(ExperimentalMaterialApi::class)

package ui

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import domain.model.sensor.NodeStatus
import domain.model.sensor.Status
import org.ocpsoft.prettytime.PrettyTime
import java.util.*

@Composable
fun NodeList(nodeStatus: SnapshotStateList<NodeStatus>) {
    Column {
        ListBody(
            nodeStatus
        )
    }
}

@Composable
fun ListBody(
    nodeStatus: SnapshotStateList<NodeStatus>
) {
    val listState = rememberLazyListState()
    LazyColumn(state = listState) {
        items(nodeStatus.sortedByDescending { it.timestamp }.distinctBy { it.node },
            key = { message -> message.node }) {
            ListItem(it)
        }
    }

}

@Composable
fun ListItem(iss: NodeStatus) {
    CustomCard(
        {
            CardBody(iss)
        }, onClick = {
            WindowsManager.currentWindows.add(ConfigWindow(iss))
        }
    )
}

@Composable
fun CardBody(x: NodeStatus) {
    CardRow {
        Column {
            Row {
                CreatedAt(x)
                Spacer(Modifier.width(10.dp))
                Node(x)
            }

            Title(x)
        }
    }
}

@Composable
fun Title(x: NodeStatus) {
    val color = if (x.status == Status.ONLINE) Color.Green else Color.Red
    Text(buildAnnotatedString {
        append("${x.node}  -->  ")
        withStyle(style = SpanStyle(fontWeight = FontWeight.Bold, color = color)) {
            append("${x.status}")
        }
    })
}

@Composable
fun CreatedAt(x: NodeStatus) {
    Text(text = timePrinter.format(Date(x.timestamp)), style = ISSUE_DATE_STYLE)
}

private val timePrinter = PrettyTime()
private val ISSUE_DATE_STYLE = TextStyle(color = Color.Gray, fontStyle = FontStyle.Italic)

@Composable
fun Node(x: NodeStatus) {
    Text(text = "#${x.node}")
}