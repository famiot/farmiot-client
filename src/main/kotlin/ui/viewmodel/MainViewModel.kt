package ui.viewmodel

import com.google.gson.JsonObject
import di.*
import domain.actions.*
import domain.model.Result
import domain.model.sensor.NodeStatus
import domain.model.sensor.SensorRead
import utils.Log
import utils.fromMap

class MainViewModel(
    private val connectAction: Connect = connect,
    private val subscribeReadAction: SubscribeRead = subscribeTopic,
    private val disconnectAction: Disconnect = disconnect,
    private val addReadingAction: AddReading = addReading,
    private val subscribeStatusAction: SubscribeStatus,
    private val subscribeConfAction: SubscribeConf = subscribeConf,
    private val publishMessageAction: PublishMessage = publishMessage,
) {
    var readCallback: ((sensorRead: SensorRead) -> Unit)? = null
    var statusCallback: ((nodeStatus: NodeStatus) -> Unit)? = null
    fun start(callback: (result: Result) -> Unit) {
        connectAction {
            callback(it)
            subscribeToStatus()
            subscribeToReads()
        }
    }

    fun stop(callback: (result: Result) -> Unit) {
        disconnectAction {
            callback(it)
        }
    }

    private fun subscribeToStatus() {
        subscribeStatusAction("+/status") { nodeStatus ->
            nodeStatus?.let { statusCallback?.invoke(it) }
        }
    }

    private fun subscribeToReads() {
        subscribeReadAction("+/read/+") { sensorRead ->
            sensorRead?.let {
                readCallback?.invoke(it)
                addReadingAction(it)
            }
        }
    }

    fun subscribeToConf(node: String, callback: ((conf: Map<String, Any>) -> Unit)) {
        subscribeConfAction("$node/conf") { conf ->
            conf?.let {
                callback.invoke(it)
            }
        }
    }

    fun publishConf(node: String, conf: Map<String, Any>, callback: (success: Boolean) -> Unit) {
        val jsonObj = JsonObject().fromMap(conf)
        val payload = jsonObj.toString()
        Log.i("publishConf", "$payload")
        publishMessageAction("$node/conf", payload) {
            callback.invoke(it)
        }
    }

    fun onRead(callback: (sensorRead: SensorRead) -> Unit) {
        readCallback = callback
    }

    fun onStatus(callback: (nodeStatus: NodeStatus) -> Unit) {
        statusCallback = callback
    }
}