package ui

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Button
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateMapOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.snapshots.SnapshotStateMap
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.ApplicationScope
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.rememberWindowState
import di.mainViewModel
import domain.model.sensor.NodeStatus
import utils.Log
import utils.toExpectedValue

@Composable
fun ApplicationScope.Config(node: NodeStatus) {
    Window(
        onCloseRequest = { WindowsManager.removeByType(WindowType.CONFIG) }, title = "Conf ${node.node}",
        state = rememberWindowState(width = 640.dp, height = 640.dp),
    ) {
        ShowConf(node.node)
    }
}

@Composable
fun ShowConf(nodeName: String) {
    val conf = remember { mutableStateMapOf<String, Any>() }
    val confSend = mutableMapOf<String, Pair<Any, String>>()
    Column {
        conf.forEach { (key, value) ->
            confSend[key] = Pair(value, value.toString())
            ConfItem(key, value) {
                confSend[key] = Pair(value, it.text)
            }
        }
        Button(modifier = Modifier.align(Alignment.CenterHorizontally), onClick = {
            if (isValidInput(confSend, conf)) {
                val map = confSend.mapValues { it.value.second.toExpectedValue(it.value.first) }
                mainViewModel.publishConf(nodeName, map) {

                }
            }
        }) {
            Text("Save")
        }
    }

    mainViewModel.subscribeToConf(nodeName) { confMap ->
        conf.putAll(confMap)
    }
}

private fun isValidInput(
    confSend: MutableMap<String, Pair<Any, String>>,
    conf: SnapshotStateMap<String, Any>
) = confSend.filter { it.value.second.isExpectedValue(it.value.first) }.size == conf.size

private fun String.isExpectedValue(value: Any): Boolean {
    return try {
        when (value) {
            is Int -> this.toInt()
            is Boolean -> this.toBooleanStrict()
            is Float -> this.toFloat()
            is Double -> this.toDouble()
        }
        true
    } catch (e: java.lang.Exception) {
        Log.e("IsExpectedValue", "${e.message}")
        false
    }
}

@Composable
fun ConfItem(key: String, value: Any, onValueChange: (TextFieldValue) -> Unit) {
    val text = remember { mutableStateOf(TextFieldValue(value.toString())) }
    val isError = remember { mutableStateOf(false) }
    onValueChange.invoke(text.value)
    OutlinedTextField(
        value = text.value.text,
        onValueChange = {
            isError.value = !it.isExpectedValue(value)
            text.value = TextFieldValue(it)
        },
        label = { Text(key) },
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
        isError = isError.value
    )
}