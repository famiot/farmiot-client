import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.*
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.ApplicationScope
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import androidx.compose.ui.window.rememberWindowState
import di.*
import domain.model.Result
import domain.model.sensor.NodeStatus
import domain.model.sensor.SensorRead
import kotlinx.coroutines.runBlocking
import ui.*

@Preview()
@Composable
fun App(isStarted: Boolean) {
    val sensorReads = remember { mutableStateListOf<SensorRead>() }
    val nodeStatus = remember { mutableStateListOf<NodeStatus>() }
    mainViewModel.onRead { sensorRead ->
        sensorReads.removeIf { it.node == sensorRead.node && it.sensor == sensorRead.sensor }
        sensorReads.add(sensorRead)
    }
    mainViewModel.onStatus {
        nodeStatus.add(it)
    }
    MaterialTheme(colors = lightThemeColors) {
        val started = remember { mutableStateOf(isStarted) }
        Column(Modifier.fillMaxSize(), Arrangement.spacedBy(5.dp)) {
            if (!started.value)
                Button(modifier = Modifier.align(Alignment.CenterHorizontally), onClick = {
                    mainViewModel.start { result ->
                        if (result is Result.Success<*>) {
                            started.value = true
                        }
                    }
                }) {
                    Text("Start")
                }
            if (started.value) {
                //readings(sensorReads, nodeStatus)
                SingleColumnLayout(sensorReads, nodeStatus, started)
            }
        }
    }
}

fun main() = application {
    WindowsManager.currentWindows.forEach {
        when (it) {
            is MainWindow -> Main(it.isStarted)
            is ConfigWindow -> Config(it.node)
        }
    }
}

@Composable
fun ApplicationScope.Main(isStarted: Boolean) {
    Window(
        onCloseRequest = ::exitApplication, title = "Farm IoT",
        state = rememberWindowState(width = 640.dp, height = 640.dp),
    ) {
        App(isStarted)
    }
}


@Composable
fun SingleColumnLayout(
    sensorReads: SnapshotStateList<SensorRead>,
    nodeStatus: SnapshotStateList<NodeStatus>,
    started: MutableState<Boolean>
) {
    val tabState = remember { mutableStateOf(TabState.READING) }
    val scroll = rememberScrollState()
    Column {
        Scaffold(
            topBar = {
                TopAppBar(
                    title = {
                        Text(
                            text = "Sensors",
                            style = MaterialTheme.typography.h5
                        )
                    },
                    navigationIcon = {
                        Button(onClick = {
                            mainViewModel.stop {
                                started.value = false
                            }
                        }) {
                            Icon(
                                Icons.Filled.ArrowBack,
                                contentDescription = "Back",
                                modifier = Modifier.size(ButtonDefaults.IconSize)
                            )
                        }
                    }
                )
            },
            content = {
                //tabs(tabState, scroll, sensorReads, nodeStatus)
                divideView(sensorReads, nodeStatus)
            }
        )
    }
}

@Composable
private fun divideView(
    sensorReads: SnapshotStateList<SensorRead>,
    nodeStatus: SnapshotStateList<NodeStatus>
) {
    Row(
        modifier = Modifier.fillMaxSize(),
        horizontalArrangement = Arrangement.SpaceEvenly,
        verticalAlignment = Alignment.Top
    ) {
        val columnModifier = Modifier.width(300.dp).fillMaxHeight().padding(5.dp)
        val titleStyle = SpanStyle(color = Color.Blue, fontWeight = FontWeight.Bold, fontSize = 18.sp)
        val titleModifier = Modifier.padding(start = 5.dp, end = 5.dp, top = 10.dp, bottom = 10.dp)
        Column(modifier = columnModifier) {
            Text(
                buildAnnotatedString { withStyle(style = titleStyle) { append("Estado de nodos") } },
                modifier = titleModifier
            )
            NodeList(nodeStatus)
        }
        Divider(modifier = Modifier.fillMaxHeight().width(3.dp), color = Color.DarkGray)
        Column(modifier = columnModifier) {
            Text(
                buildAnnotatedString { withStyle(style = titleStyle) { append("Lectura de sensores") } },
                modifier = titleModifier
            )
            ReadingList(sensorReads)
        }
    }
}

@Composable
private fun tabs(
    tabState: MutableState<TabState>,
    scroll: ScrollState,
    sensorReads: SnapshotStateList<SensorRead>,
    nodeStatus: SnapshotStateList<NodeStatus>
) {
    Column {
        TabRow(selectedTabIndex = tabState.value.ordinal) {
            getTab(tabState, scroll, "Readings", TabState.READING)
            getTab(tabState, scroll, "Nodes", TabState.NODE)
        }
        when (tabState.value) {
            TabState.READING -> ReadingList(sensorReads)
            TabState.NODE -> NodeList(nodeStatus)
        }
    }
}

@Composable
private fun getTab(
    tabState: MutableState<TabState>,
    scroll: ScrollState,
    text: String,
    state: TabState
) {
    Tab(
        text = { Text(text) },
        selected = tabState.value == state,
        onClick = {
            tabState.value = state
            runBlocking {
                scroll.scrollTo(0)
            }
        }
    )
}

enum class TabState {
    READING,
    NODE
}

val lightThemeColors = lightColors(
    primary = Color(0xFF073763),
    primaryVariant = Color(0xFF6a87a1),
    secondary = Color.White,
    error = Color(0xFFD00036)
)

