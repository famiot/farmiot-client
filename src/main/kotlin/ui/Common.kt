@file:OptIn(ExperimentalMaterialApi::class)

package ui

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@ExperimentalMaterialApi
@Composable
fun CustomCard(content: @Composable () -> Unit, onClick: () -> Unit = {}) {
    Card(
        onClick = onClick,
        modifier = Modifier.padding(5.dp).fillMaxWidth(),
        elevation = 5.dp,
        shape = RoundedCornerShape(10.dp),
        content = content
    )
}

@Composable
fun CardRow(content: @Composable() (RowScope.() -> Unit)) {
    Row(
        modifier = Modifier.padding(10.dp).fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceBetween,
        content = content
    )
}