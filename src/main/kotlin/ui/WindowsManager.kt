package ui

import androidx.compose.runtime.mutableStateListOf
import domain.model.sensor.NodeStatus

object WindowsManager {
    var currentWindows = mutableStateListOf<Window>()

    init {
        currentWindows += MainWindow()
    }

    fun add(window: Window) {
        currentWindows += window
    }

    fun removeByType(type: WindowType) {
        currentWindows.removeIf { it.type == type }
    }
}

open class Window(val type: WindowType)

class MainWindow(val isStarted: Boolean = false) : Window(WindowType.MAIN)

class ConfigWindow(val node: NodeStatus) : Window(WindowType.CONFIG)

enum class WindowType {
    MAIN,
    CONFIG
}