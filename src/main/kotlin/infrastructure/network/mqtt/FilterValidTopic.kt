@file:Suppress("BooleanMethodIsAlwaysInverted")

package infrastructure.network.mqtt

class FilterValidTopic {
    operator fun invoke(
        key: String,
        topic: String
    ): Boolean {
        val subTopic = key.split('/')
        val messageTopic = topic.split('/')
        var valid = false
        subTopic.forEachIndexed { index: Int, s: String ->
            valid = s == "+" || messageTopic[index] == s
        }
        return valid
    }

}