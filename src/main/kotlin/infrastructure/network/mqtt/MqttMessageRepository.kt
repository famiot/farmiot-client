package infrastructure.network.mqtt

import com.fasterxml.jackson.core.JsonParseException
import domain.MessageRepository
import domain.model.Result
import org.eclipse.paho.client.mqttv3.*
import utils.LogHelper
import java.nio.charset.Charset
import java.util.concurrent.ConcurrentHashMap

class MqttMessageRepository(private val client: IMqttAsyncClient) : MessageRepository, IMqttActionListener,
    MqttCallbackExtended {
    private val filterValidTopic: FilterValidTopic = FilterValidTopic()
    private val subscribers = ConcurrentHashMap<String, (Result) -> Unit>()

    override fun connect(callback: (result: Result) -> Unit) {
        client.connect(getMqttConnectOptions(true), UserAction(MessageAction.CONNECT, callback), this)
        client.setCallback(this)
    }

    override fun disconnect(callback: (result: Result) -> Unit) {
        client.disconnect(MessageAction.DISCONNECT, this)
    }

    override fun isConnected(): Boolean {
        return client.isConnected
    }

    override fun publish(topic: String, payload: String, callback: (Result) -> Unit) {
        client.publish(
            topic,
            payload.toByteArray(Charset.forName("UTF8")),
            0,
            false,
            UserAction(MessageAction.PUBLISH, callback),
            this
        )
    }

    override fun subscribe(topic: String, callback: (message: Result) -> Unit) {
        subscribers[topic] = callback
        subscribeToTopic(topic)
    }

    private fun subscribeToTopic(topic: String) {
        val qos = arrayListOf<Int>().also { it.add(2) }
        val topics = arrayListOf<String>().also { it.add(topic) }
        client.subscribe(topics.toTypedArray(), qos.toIntArray(), topic, this)
    }

    override fun unsubscribe(topic: String) {
        subscribers.remove(topic)
    }

    private fun getMqttConnectOptions(cleanSession: Boolean): MqttConnectOptions {
        val mqttConnectOptions = MqttConnectOptions()
        with(mqttConnectOptions) {
            isAutomaticReconnect = true
            isCleanSession = cleanSession
            maxInflight = 100
            isHttpsHostnameVerificationEnabled = false
        }
        return mqttConnectOptions
    }

    override fun onSuccess(asyncActionToken: IMqttToken) {
        LogHelper.i("Mqtt", "onSuccess: ${asyncActionToken.userContext}")
        val userAction = asyncActionToken.userContext
        if (userAction is UserAction) {
            manageSuccessUserAction(userAction)
        }

    }

    override fun onFailure(asyncActionToken: IMqttToken, exception: Throwable) {
        LogHelper.i("Mqtt", "onFailure: ${asyncActionToken.userContext}")
        val userAction = asyncActionToken.userContext
        if (userAction is UserAction) {
            manageFailureUserAction(userAction, exception)
        }
    }

    private fun manageFailureUserAction(userAction: UserAction, exception: Throwable) {
        when (userAction.messageAction) {
            MessageAction.CONNECT, MessageAction.DISCONNECT -> userAction.callback(Result.Failure(exception))
            MessageAction.PUBLISH -> userAction.callback(Result.Failure(exception))
        }
    }

    private fun manageSuccessUserAction(userAction: UserAction) {
        when (userAction.messageAction) {
            MessageAction.CONNECT, MessageAction.DISCONNECT -> userAction.callback(Result.Success(true))
            MessageAction.PUBLISH -> userAction.callback(Result.Success(true))
        }
    }

    override fun connectionLost(cause: Throwable?) {
        LogHelper.i("Mqtt", "connectionLost: $cause")
    }

    override fun messageArrived(topic: String, message: MqttMessage) {
        LogHelper.i("Mqtt", "messageArrived: $topic -> ${String(message.payload)}")
        try {
            message.payload?.let { payload ->
                subscribers.filter { filterValidTopic(it.key, topic) }.values.forEach {
                    it.invoke(Result.Success(String(payload)))
                }
            }
        } catch (e: JsonParseException) {
            LogHelper.e("Mqtt", "JsonParseException -> ${e.message}")
        }
    }

    override fun deliveryComplete(token: IMqttDeliveryToken?) {
        LogHelper.i("Mqtt", "deliveryComplete")
    }

    override fun connectComplete(reconnect: Boolean, serverURI: String?) {
        LogHelper.i("Mqtt", "connectComplete: $serverURI -> reconnect $reconnect")
        if (reconnect) {
            subscribers.keys().toList().forEach {
                subscribeToTopic(it)
            }
        }
    }
}

enum class MessageAction {
    CONNECT,
    DISCONNECT,
    PUBLISH
}

data class UserAction(val messageAction: MessageAction, val callback: (result: Result) -> Unit)