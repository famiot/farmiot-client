package infrastructure.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class NodeConfig(@JsonProperty("wait_time") val waitTime: Long)
