package infrastructure.dto

import domain.model.sensor.NodeStatus
import domain.model.sensor.Status
import java.util.*

data class NodeStatusDto(
    val node: String,
    val status: String,
    val sensors: List<String> = emptyList(),
    val uptime: Long,
    val wifi_rssi: Int,
    val ip_address: String,
) {
    fun toNodeStatus(): NodeStatus {
        return NodeStatus(Date().time, this.node, this.status.toStatus())
    }
}

private fun String.toStatus(): Status {
    return when (this) {
        "ONLINE" -> Status.ONLINE
        "OFFLINE" -> Status.OFFLINE
        else -> Status.UNKNOWN
    }
}
