package infrastructure.dto

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.module.kotlin.MissingKotlinParameterException
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import domain.model.sensor.SensorRead
import java.util.*

data class SensorReadDto(val sensor: String, val node: String, val data: Data) {
    fun toSensorRead(): SensorRead {
        return SensorRead(Date().time, this.node, this.sensor, data.value)
    }
}

data class Data(val type: Type, val value: Any)

enum class Type {
    @JsonProperty("float")
    FLOAT,

    @JsonProperty("string")
    STRING,

    @JsonProperty("double")
    DOUBLE,

    @JsonProperty("boolean")
    BOOLEAN,

    @JsonProperty("int")
    INT;
}

fun String.toSensorReadDto(): SensorReadDto? {
    return try {
        jacksonObjectMapper().readValue(this)
    } catch (e: MissingKotlinParameterException) {
        null
    }
}