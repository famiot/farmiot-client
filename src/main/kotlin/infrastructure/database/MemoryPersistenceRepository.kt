package infrastructure.database

import domain.PersistenceRepository
import domain.model.sensor.Sensor
import domain.model.sensor.SensorRead

class MemoryPersistenceRepository(private val maxRecords: Long = 5) : PersistenceRepository {

    private val readings = mutableListOf<SensorRead>()

    override fun addReading(vararg sensorRead: SensorRead) {
        readings.addAll(sensorRead)
    }

    override fun readingsBySensor(sensor: Sensor): List<SensorRead> {
        return readings.filter { it.sensor == sensor }
    }

    override fun readingsByDate(from: Long, to: Long): List<SensorRead> {
        return readings.filter { it.timestamp in from..to }
    }

    override fun dump(): List<SensorRead> {
        val clone = readings.toList()
        readings.clear()
        return clone
    }

    override fun isFull(): Boolean = readings.size >= maxRecords
}