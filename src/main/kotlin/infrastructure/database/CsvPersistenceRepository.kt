package infrastructure.database

import com.fasterxml.jackson.dataformat.csv.CsvMapper
import com.fasterxml.jackson.dataformat.csv.CsvSchema
import com.fasterxml.jackson.module.kotlin.KotlinFeature
import com.fasterxml.jackson.module.kotlin.KotlinModule
import di.logger
import domain.PersistenceRepository
import domain.model.sensor.Sensor
import domain.model.sensor.SensorRead
import utils.getStringFromEpoch
import utils.isBetweenTimes
import java.io.File
import java.io.FileOutputStream
import java.io.FileReader

class CsvPersistenceRepository(folderStr: String) :
    PersistenceRepository {

    private val folder = File(folderStr).also {
        if (!it.exists()) it.mkdir()
    }

    private val csvMapper = CsvMapper().apply {
        registerModule(
            KotlinModule.Builder()
                .withReflectionCacheSize(512)
                .configure(KotlinFeature.NullToEmptyCollection, false)
                .configure(KotlinFeature.NullToEmptyMap, false)
                .configure(KotlinFeature.NullIsSameAsDefault, false)
                .configure(KotlinFeature.SingletonSupport, true)
                .configure(KotlinFeature.StrictNullChecks, false)
                .build()
        )
    }

    override fun addReading(vararg sensorRead: SensorRead) {
        val data = sensorRead.asList()
        if (data.isNotEmpty())
            appendCsvFile(
                data,
                getReadingFilePath(data)
            )
    }

    override fun readingsBySensor(sensor: Sensor): List<SensorRead> {
        val files = folder.walk().filter { it.isFile }
        return files.map { readCsvFile<SensorRead>(it.absolutePath) }.flatMap { it.toList() }.toList()
            .filter { it.sensor == sensor }
    }

    override fun readingsByDate(from: Long, to: Long): List<SensorRead> {
        val files = folder.walk().filter {
            it.isFile && it.name.split('.').first().isBetweenTimes(from, to)
        }

        return files.map {
            readCsvFile<SensorRead>(it.absolutePath)
        }.flatMap { it.toList() }.toList()
            .filter { it.timestamp in from..to }
    }

    override fun dump(): List<SensorRead> {
        folder.deleteRecursively()
        return emptyList()
    }

    override fun isFull(): Boolean {
        return folder.usableSpace < 100000
    }

    private fun getReadingFilePath(data: List<SensorRead>): String {
        val base = "${folder.absolutePath}${File.separatorChar}"
        return "$base${data.first().timestamp.getStringFromEpoch()}.csv"
    }

    private inline fun <reified T> appendCsvFile(data: List<T>, fileName: String) {
        logger.i("CsvPersistenceRepository", "appendCsvFile: $fileName")
        val schema = getAppendSchema<T>(fileName)
        FileOutputStream(fileName, true).use { writer ->
            csvMapper.writer(schema)
                .writeValues(writer)
                .writeAll(data)
                .close()
        }
    }

    private inline fun <reified T> getAppendSchema(
        fileName: String
    ): CsvSchema {
        val outputFile = File(fileName)
        val schema = if (!outputFile.exists()) {
            csvMapper.schemaFor(T::class.java).withHeader()
        } else {
            csvMapper.schemaFor(T::class.java)
        }
        return schema
    }

    private inline fun <reified T> readCsvFile(fileName: String): List<T> {
        FileReader(fileName).use { reader ->
            return csvMapper
                .readerFor(T::class.java)
                .with(CsvSchema.emptySchema().withHeader())
                .readValues<T>(reader)
                .readAll()
                .toList()
        }
    }
}