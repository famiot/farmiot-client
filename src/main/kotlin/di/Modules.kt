package di

import domain.actions.*
import infrastructure.database.CsvPersistenceRepository
import infrastructure.network.mqtt.Constants
import infrastructure.network.mqtt.MqttMessageRepository
import org.eclipse.paho.client.mqttv3.MqttAsyncClient
import ui.viewmodel.MainViewModel
import utils.LogHelper

val mqttMessageRepository = MqttMessageRepository(MqttAsyncClient(Constants.MQTT_SERVER, "User1"))
val connect: Connect = ConnectAction(mqttMessageRepository)
val disconnect: Disconnect = DisconnectAction(mqttMessageRepository)
val publishMessage: PublishMessage = PublishMessageAction(mqttMessageRepository)
val subscribeTopic: SubscribeRead = SubscribeReadAction(mqttMessageRepository)
val subscribeStatus: SubscribeStatus = SubscribeStatusAction(mqttMessageRepository)
val subscribeConf: SubscribeConf = SubscribeConfAction(mqttMessageRepository)
val addReading: AddReading = AddReading(persistenceRepository = CsvPersistenceRepository("Readings"))
val logger: LogHelper = LogHelper
val mainViewModel: MainViewModel = MainViewModel(subscribeStatusAction = subscribeStatus)